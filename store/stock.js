/**
 * ================================================================
 * state
 * プログラムの都合上stateのkeyはsnake_caseにしている
 * ================================================================
 */
export const state = () => ({
  // カブ価
  price_sun: null,
  price_monam: null,
  price_monpm: null,
  price_tueam: null,
  price_tuepm: null,
  price_wedam: null,
  price_wedpm: null,
  price_thuam: null,
  price_thupm: null,
  price_friam: null,
  price_fripm: null,
  price_satam: null,
  price_satpm: null,

  // 保有カブ数
  amount: null,

  // 島名
  name: null
})

/**
 * ================================================================
 * getters
 * ================================================================
 */

export const getters = {
  /**
   * 単純なStateの取得
   */
  amount: state => state.amount,
  name: state => state.name,

  /**
   * カブ価
   */
  prices: (state) => {
    return {
      sun: state.price_sun,
      monam: state.price_monam,
      monpm: state.price_monpm,
      tueam: state.price_tueam,
      tuepm: state.price_tuepm,
      wedam: state.price_wedam,
      wedpm: state.price_wedpm,
      thuam: state.price_thuam,
      thupm: state.price_thupm,
      friam: state.price_friam,
      fripm: state.price_fripm,
      satam: state.price_satam,
      satpm: state.price_satpm
    }
  },

  /**
   * ----------------------------------------------------------------
   * 最新カブ価
   * ----------------------------------------------------------------
   */

  /**
   * 最新および準最新カブ価
   */
  latestPrices: (state) => {
    // 最新カブ価, 準最新カブ価, 最新カブ価のkey の初期値を宣言する
    let firstLatestPrice = null
    let secondLatestPrice = null
    let latestPriceKey = null

    // 時系列順のカブ価のkey
    const priceKeys = {
      0: 'price_sun',
      1: 'price_monam',
      2: 'price_monpm',
      3: 'price_tueam',
      4: 'price_tuepm',
      5: 'price_wedam',
      6: 'price_wedpm',
      7: 'price_thuam',
      8: 'price_thupm',
      9: 'price_friam',
      10: 'price_fripm',
      11: 'price_satam',
      12: 'price_satpm'
    }

    // 時系列順にカブ価を取得する
    for (const no in priceKeys) {
      const priceKey = priceKeys[no]
      const price = state[priceKey]

      if (price != null) {
        // 準最新カブ価: 以前までの最新カブ価
        secondLatestPrice = firstLatestPrice
        // 最新カブ価
        firstLatestPrice = price
        // 最新カブ価のkey
        latestPriceKey = priceKey
      }
    }

    // 最新カブ価のkeyが取得できた場合、keyから 'price_' を取り除く
    // 例: 'price_monam' => 'monam'
    if (latestPriceKey != null) {
      latestPriceKey = latestPriceKey.replace('price_', '')
    }

    return {
      first: firstLatestPrice,
      second: secondLatestPrice,
      key: latestPriceKey
    }
  },

  /**
   * 最新カブ価変動値
   */
  latestPriceDifference: (state, getters) => {
    // 最新カブ価, 準最新カブ価 を取得する
    const first = getters.latestPrices.first
    const second = getters.latestPrices.second

    if (first == null || second == null) {
      // 最新カブ価, 準最新カブ価 のいずれかがnullの場合、nullを返す
      return null
    } else {
      // 最新カブ価変動値 = 最新カブ価 - 準最新カブ価
      return first - second
    }
  },

  /**
   * 最新カブ価変動割合
   */
  latestPricePercentage: (state, getters) => {
    // 準最新カブ価, 最新カブ価変動値 を取得する
    const second = getters.latestPrices.second
    const difference = getters.latestPriceDifference

    if (second == null || difference == null) {
      // 準最新カブ価, 最新カブ価変動値 のいずれかがnullの場合、nullを返す
      return null
    } else {
      // 最新カブ価変動割合 = 最新カブ価変動値 / 準最新カブ価 * 100
      return difference / second * 100
    }
  },

  /**
   * 最新カブ価が入力されている曜日
   */
  latestPriceWeek: (state, getters) => {
    // 最新カブ価のkeyを取得する
    const key = getters.latestPrices.key

    if (key == null) {
      // keyがnullの場合、nullを返す
      return null
    } else if (key == 'sun') {
      // keyが 'sun' の場合、 'sun' を返す
      return key
    } else {
      // keyが 'sun' 以外の場合: key末尾の 'am' もしくは 'pm' を取り除いて返す
      return key.replace('am', '').replace('pm', '')
    }
  },

  /**
   * OHLC (始, 高, 安, 終)
   */
  openHighLowClose: (state) => {
    // OHLCの初期値を宣言する
    const openHighLowClose = {
      sun: null,
      mon: null,
      tue: null,
      wed: null,
      thu: null,
      fri: null,
      sat: null
    }

    // 曜日のkey
    const weekList = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']

    // 曜日ごとの始値のkey
    const openKeyList = {
      mon: 'price_sun',
      tue: 'price_monpm',
      wed: 'price_tuepm',
      thu: 'price_wedpm',
      fri: 'price_thupm',
      sat: 'price_fripm'
    }

    for (const week of weekList) {      
      if (week == 'sun') {
        // SUN
        const open = 100
        const price = state.price_sun
        const high = Math.max(open, price)
        const low = Math.min(open, price)

        openHighLowClose[week] = {
          open: open,
          high: high,
          low: low,
          close: price
        }
      } else {
        // MON-SAT
        const open = state[openKeyList[week]]
        const priceAm = state['price_' + week + 'am']
        const pricePm = state['price_' + week + 'pm']

        // 比較に使用する数値の配列を作成する
        // TODO: リファクタ
        const priceList = []
        for (const p of [open, priceAm, pricePm]) {
          if (p != null) {
            priceList.push(p)
          }
        }

        const high = Math.max.apply(null, priceList)
        const low = Math.min.apply(null, priceList)
  
        openHighLowClose[week] = {
          open: open,
          high: high,
          low: low,
          close: pricePm,
        }
      }
    }

    return openHighLowClose
  },

  /**
   * ----------------------------------------------------------------
   * 取得価格合計
   * ----------------------------------------------------------------
   */

  /**
   * 取得価格合計
   */
  totalPrice: (state, getters) => {
    // 取得単価, 保有カブ数 を取得する
    const getPrice = state.price_sun
    const amount = state.amount

    if (getPrice == null || amount == null) {
      // 取得単価, 保有カブ数 のいずれかがnullの場合、nullを返す
      return null
    } else {
      // 取得価格合計 = 評価損益合計 / 取得価格合計 * 100
      return getPrice * amount
    }
  },

  /**
   * ----------------------------------------------------------------
   * 評価損益合計
   * ----------------------------------------------------------------
   */

  /**
   * 評価損益合計
   */
  totalProfit: (state, getters) => {
    // 最新カブ価, 取得単価, 保有カブ数 を取得する
    const latestPrice = getters.latestPrices.first
    const getPrice = state.price_sun
    const amount = state.amount

    if (latestPrice == null || getPrice == null || amount == null) {
      // 最新カブ価, 取得単価, 保有カブ数 のいずれかがnullの場合、nullを返す
      return null
    } else {
      // 評価損益合計 = (最新カブ価 - 取得単価) * 保有カブ数
      return (latestPrice - getPrice) * amount
    }
  },

  /**
   * 評価損益合計変動割合
   */
  totalProfitPercentage: (state, getters) => {
    // 取得価格合計, 評価損益合計 を取得する
    const totalPrice = getters.totalPrice
    const totalProfit = getters.totalProfit

    if (totalPrice == null || totalProfit == null) {
      // 取得価格合計, 評価損益合計 のいずれかがnullの場合、nullを返す
      return null
    } else {
      // 評価損益合計変動割合 = 評価損益合計 / 取得価格合計 * 100
      return totalProfit / totalPrice * 100
    }
  }
}

/**
 * ================================================================
 * mutations
 * ================================================================
 */

export const mutations = {
  /**
   * カブ価を設定する
   *
   * @param {number} price カブ価
   * @param {string} week 曜日 (例: 'mon')
   * @param {string} ampm 午前午後 (例: 'am') 日曜日の場合は省略可能
   */
  setPrice (state, { price, week, ampm = '' }) {
    // 例: state['price_monam'] = 100
    state['price_' + week + ampm] = price
  },

  /**
   * すべてのカブ価を初期値に戻す
   */
  resetPrices (state) {
    state.price_sun = null
    state.price_monam = null
    state.price_monpm = null
    state.price_tueam = null
    state.price_tuepm = null
    state.price_wedam = null
    state.price_wedpm = null
    state.price_thuam = null
    state.price_thupm = null
    state.price_friam = null
    state.price_fripm = null
    state.price_satam = null
    state.price_satpm = null
  },

  /**
   * 保有カブ数を設定する
   *
   * @param {number} amount 保有カブ数
   */
  setAmount (state, { amount }) {
    state.amount = amount
  },

  /**
   * 保有カブ数を初期値に戻す
   */
  resetAmount (state) {
    state.amount = null
  },

  /**
   * 島名を設定する
   *
   * @param {string} name 島名
   */
  setName (state, { name }) {
    state.name = name
  }
}

/**
 * ================================================================
 * actions
 * ================================================================
 */

export const actions = {
  /**
   * カブ価を設定する
   */
  setPrice ({ commit }, { price, week, ampm }) {
    commit('setPrice', { price, week, ampm })
  },

  /**
   * すべてのカブ価を初期値に戻す
   */
  resetPrices ({ commit }) {
    commit('resetPrices')
  },

  /**
   * 保有株数を設定する
   */
  setAmount ({ commit }, { amount }) {
    commit('setAmount', { amount })
  },

  /**
   * 保有株数を初期値に戻す
   */
  resetAmount ({ commit }) {
    commit('resetAmount')
  },

  /**
   * 島名を設定する
   */
  setName ({ commit }, { name }) {
    commit('setName', { name })
  }
}
