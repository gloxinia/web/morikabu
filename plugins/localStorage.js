// ref. https://github.com/robinvdvleuten/vuex-persistedstate#nuxtjs

import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
        key: 'stock',
    })(store)
  })
}
